package com.company.test;

import com.company.CommandCenter;
import com.company.Spaceship;
import com.company.SpaceshipFleetManager;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {

    SpaceshipFleetManager center;
    //что проверяемv
    ArrayList<Spaceship> testList = new ArrayList<>();

    public static void main(String[] args) {

        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());
        double score = 0;

        boolean test1 = spaceshipFleetManagerTest.getShipByName_shipExists_returnTargetShip();


        if (test1) {
            System.out.println("Test 1 is successful");
            score+=0.45;
        } else {
            System.out.println("Test 1 failed");

        }

        boolean test2 = spaceshipFleetManagerTest.getShipByName_shipNotExists_returnNull();

        if (test2) {
            System.out.println("Test 2 is successful");
            score=score+0.45;
        } else {
            System.out.println("Test 2 failed");
        }

        boolean test3 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_ArrayWasNotShipWithEnoughCargoSpace_returnEmptyList();

        if (test3) {
            System.out.println("Test 3 is successful");
            score=score+0.45;
        } else {
            System.out.println("Test 3 failed");
        }
        boolean test4 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_ArrayWasShipWithEnoughCargoSpace_returnListWithSpaceshipsWhichHasEnoughCargoSize();

        if (test4) {
            System.out.println("Test 4  is successful");
            score=score+0.45;
        } else {
            System.out.println("Test 4 failed");
        }

        boolean test5 = spaceshipFleetManagerTest.getAllCivilianShips_peacefulShips_returnEmptyShips();

        if (test5) {
            System.out.println("Test 5 is successful");
            score=score+0.45;
        } else {
            System.out.println("Test 5 is successful");
        }
        boolean test6 = spaceshipFleetManagerTest.getAllCivilianShips_peacefulShips_returnNotEmptyShips();

        if (test6) {
            System.out.println("Test 6 is successful");
            score=score+0.45;
        } else {
            System.out.println("Test 6 is successful");
        }

        boolean test7 = spaceshipFleetManagerTest.getMostPowerfulShip_ArrayWithMostPowerfulShip_returnShip();

        if (test7){
            System.out.println("Test 7 is successful");
            score=score+0.45;
        } else {
            System.out.println("Test 7 failed");
        }

        boolean test8 = spaceshipFleetManagerTest.getMostPowerfulShip_firstShipIsMostPowerful_returnShip();

        if (test8){
            System.out.println("Test 8 is successful");
            score=score+0.45;
        } else{
            System.out.println("Test 8 failed");
        }

        boolean test9 = spaceshipFleetManagerTest.getMostPowerfulShip_ShipWithoutPower_returnNull();

        if (test9){
            System.out.println("Test 9 is successful");
            score=score+0.45;
        } else {
            System.out.println("Test 9 failed");
        }

        System.out.println("Score : "+score);



    }
    //test 7
    private boolean getMostPowerfulShip_ArrayWithMostPowerfulShip_returnShip(){
        ArrayList<Spaceship> mostPowerfulShip = new ArrayList<>();

        mostPowerfulShip.add(new Spaceship("ghg",100,6,7));
        mostPowerfulShip.add(new Spaceship("hvmh",20,6,7));
        mostPowerfulShip.add(new Spaceship("ghg",30,6,7));

        Spaceship result = center.getMostPowerfulShip(mostPowerfulShip);

        if(result != null){
            return true;

        } return false;
    }
    //test 8
    private boolean getMostPowerfulShip_firstShipIsMostPowerful_returnShip() {
        ArrayList<Spaceship> mostPowerfulShip = new ArrayList<>();

        mostPowerfulShip.add(new Spaceship("ghg", 100, 6, 7));
        mostPowerfulShip.add(new Spaceship("hvmh", 100, 6, 7));
        mostPowerfulShip.add(new Spaceship("ghg", 30, 6, 7));

        Spaceship result = center.getMostPowerfulShip(mostPowerfulShip);
        String testName = "hvmh";

        if (result !=null && result.getName().equals(testName)) {
            return true;
        }
        return false;
    }
    //test9
    private boolean getMostPowerfulShip_ShipWithoutPower_returnNull() {
        ArrayList<Spaceship> mostPowerfulShip = new ArrayList<>();

        mostPowerfulShip.add(new Spaceship("ghg", 0, 6, 7));
        mostPowerfulShip.add(new Spaceship("hvmh", 0, 6, 7));
        mostPowerfulShip.add(new Spaceship("ghg", 0, 6, 7));

        Spaceship result = center.getMostPowerfulShip(mostPowerfulShip);

        if(result==null){
            return true;
        } return false;
    }


    //test1
    //метод принимает на вход список кораблей и возвращает корабль по имени
    private boolean getShipByName_shipExists_returnTargetShip(){

        //кладем тестовые данные
        testList.add(new Spaceship("Foo",100,0,10));
        testList.add(new Spaceship("Bar",100,0,10));
        testList.add(new Spaceship("Mark 1 ",100,0,10));
        //вызываем метод которвый проверяем
        String testName = "Bar";
        Spaceship result = center.getShipByName(testList,testName);

        if(result!=null && result.getName().equals(testName)){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;



    }
    //test2
    //тест на негативный исход
    private boolean getShipByName_shipNotExists_returnNull(){

        //кладем тестовые данные
        testList.add(new Spaceship("Foo",100,0,10));
        testList.add(new Spaceship("Bar",100,0,10));
        testList.add(new Spaceship("Mark 1 ",100,0,10));
        //вызываем метод которвый проверяем
        String testName = "Mark 2";
        Spaceship result = center.getShipByName(testList,testName);

        if(result == null ){
            testList.clear();
            return true;
        }
        testList.clear();
        return false;


    }
    //test3
    private boolean getAllShipsWithEnoughCargoSpace_ArrayWasShipWithEnoughCargoSpace_returnListWithSpaceshipsWhichHasEnoughCargoSize(){
        ArrayList<Spaceship> cargoShips = new ArrayList<>();

        cargoShips.add(new Spaceship("ship1",10,100,4));
        cargoShips.add(new Spaceship("ship2",10,100,4));
        cargoShips.add(new Spaceship("ship3",10,100,4));

        int cargoSpace = 100;

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(cargoShips,cargoSpace);

        for(Spaceship ship: result){
            if(ship.getCargoSpace()<cargoSpace)
                return false;
        }
        return true;
    }
    //test4
    private boolean getAllShipsWithEnoughCargoSpace_ArrayWasNotShipWithEnoughCargoSpace_returnEmptyList(){
        ArrayList<Spaceship> cargoShips = new ArrayList<>();

        cargoShips.add(new Spaceship("ship1",10,100,4));
        cargoShips.add(new Spaceship("ship2",10,100,4));
        cargoShips.add(new Spaceship("ship3",10,100,4));

        int cargoSpace = 100;

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(cargoShips,cargoSpace);

        if (result.isEmpty())
            return true;

        return false;
    }
    //test6
    private boolean getAllCivilianShips_peacefulShips_returnNotEmptyShips(){
        ArrayList<Spaceship> civilianShip = new ArrayList<>();

        civilianShip.add(new Spaceship("qwe",10,10,10));
        civilianShip.add(new Spaceship("sd",10,10,10));
        civilianShip.add(new Spaceship("qsdwe",10,10,10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(civilianShip);

        if(result.size() == 0){
            return true;
        } else {
            return false;
        }
    }
    //tet5
    private boolean getAllCivilianShips_peacefulShips_returnEmptyShips(){
        ArrayList<Spaceship> civilianShip = new ArrayList<>();

        civilianShip.add(new Spaceship("qwe",10,10,10));
        civilianShip.add(new Spaceship("sd",10,10,10));
        civilianShip.add(new Spaceship("qsdwe",10,10,10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(civilianShip);

        if(result.size() != 0){
            return false;
        }
        return true;
    }








    public SpaceshipFleetManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }
}



