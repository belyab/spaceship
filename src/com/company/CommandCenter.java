package com.company;

import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {




    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        Spaceship spaceship = new Spaceship("null", 0, 0, 0);
        for (Spaceship ship : ships) {
            if (ship.getFirePower() > spaceship.getFirePower()) {
                spaceship = ship;
            }
        }
        if (spaceship.getFirePower() == 0) {
            return null;
        } else {
            return spaceship;
        }
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (Spaceship ship : ships) {
            if (ship.getName().equals(name)) {
                return ship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> shipsWithEnoughCargoSpace = new ArrayList<>();
        for (Spaceship ship : ships) {
            if (ship.getCargoSpace() >= cargoSize) {
                shipsWithEnoughCargoSpace.add(ship);
            }
        }
        return shipsWithEnoughCargoSpace;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> friendlySpaceship = new ArrayList<>();
        for (Spaceship ship : ships) {
            if (ship.getFirePower() == 0) {
                friendlySpaceship.add(ship);
            }
        }
        return friendlySpaceship;
    }
}





